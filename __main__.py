import braintree
from bottle import request, response, run, template, Bottle
import bottle_mysql


app = Bottle()
plugin = bottle_mysql.Plugin(dbuser='ba97d5dccbd22d', dbpass='04bc3876', dbname='test-braintree',
                             dbhost='br-cdbr-azure-south-a.cloudapp.net')
app.install(plugin)



braintree.Configuration.configure(
    braintree.Environment.Sandbox,
    'r8khkb5d3g39jryf',
    '7nn66mjjy7x473q7',
    '7201d1b91d1ba68208798c368f1f92d2'
)



@app.post('/')
def index(db):
    notification = braintree.WebhookNotification.parse(
        request.data['bt_signature'],
        request.data['bt_payload'].replace('\\n', '\n')
    )

    db.execute('insert into `logs` (`kind`, `timestamp`, `data`) values (%s, %s, %s)', (notification.kind, str(notification.timestamp), str(getattr(notification, 'subject', 'None'))))


    return response()


app.run(host='0.0.0.0', port=8080)